# example/urls.py
from django.urls import path

from example.views import index, kodachrome


urlpatterns = [
    path('', index),
    path('/kodachrome', kodachrome),
]